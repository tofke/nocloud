// Settings:
:toc: macro
:numbered: 1
:sectids: 1
:sectlinks: 1

= nocloud

toc::[]

<<<
:numbered:

<<<

== TL;DR

nocloud is a set of tools to run a mini-cloud on your machine based on kvm / qemu and written in bash.

To install nocloud :

 git clone https://gitlab.com/n0vember/nocloud 
 cd nocloud
 sudo make
 
To download a vm template and instanciate 5 times this template :

 nocloud template download debian9
 nocloud run machines debian9:5:deb

== Presentation

nocloud is a cheap solution to own your cloud. It was first developped as a side project to provide the tools I needed for my virtualbox usage. I switched to kvm backend but keeping in mind the possibility to offer multiple backends.

it is :

- modular (composed of multiple simple and one-task-only command-line tools)
- using kvm to manage vms
- using ansible to configure vms

it is not :

- ment to be a production cloud platform
- bug free

I therefore only have tested with Archlinux hosts and it has been reported to me to run on ubuntu. Debian, Centos and Archlinux guests were tested. And this is what will be used next as examples. Examples will be simple. Multiple options are available and are documented through the -h option of each script.

NOTE : nocloud command replaces the vm commands (except vmssh)

== System pre-requisites

Following things needs to be installed on your machine :

- qemu
- dnsmasq
- make
- tree
- socat

Note that for users to use kvm and nocloud, you must add them to the kvm group. The user proceeding to installation is added during the process. If you want to add other users :

  useradd -a -G kvm my_user

== Network

=== Configuration

The solution is based on bridging capability. Setup needs root access and I want to reduce sudo commands through the tools at its minimum. Needs are explained below.

During installation, 2 services are created :

- nocloud-bridge which creates a bridge interface and sets up firewalling rules
- nocloud-dnsmasq brings a dnsmasq server up to serve DHCP and DNS to the machines

The configuration file /etc/qemu/bridge.conf is also created during installation. It allows qemu to bring machines interfaces connected to the bridge inteface.

Installation also configures the kvm_intel and vhost_net modules.

Note : if you restart your iptables, the nocloud rules will be dropped. no-cloud-bridge service needs to be restarted as well.

== Installation

Only thing to do is :

 git clone https://gitlab.com/n0vember/nocloud
 cd nocloud
 sudo make

== Configuration

=== Configuration files

Nocloud comes with a default configuration. It is defined in /usr/local/lib/cm_lib. This configuration can be overriden using one of the following config file (searched for in this order) :

* ~/.nocloud.conf
* ~/.config/nocloud/nocloud.conf
* /etc/nocloud.conf

Those configuration files can override any parameter defined in cm_lib.

If you want to resolve machine names, you will need to add 192.168.1.1 to your DNS servers. This will be automated as much as possible in the future releases, but it may be impossible to cover all cases so you may have to do some configuration by yourself.

One of the simpliest case is systemd-resolved where you just have to add the IP to the DNS= line of /etc/systemd/resolved.conf, "nocloud" to the Domains= line and restart the service.

=== Environment variables

- NC_NO_KVM, if set to 1 changes qemu behaviour not to use kvm extensions. It will then fully emulate the VM with loss of performance. This option however allows to launch VMs on a machine where you don't have VT-x extensions or in a VM where you don't have nested capabilities.

- NC_CONTEXT sets context for all programs. Setting this variable will limit commands to this context. The goal is to ease the use of multiple contexts on the same host with no interference.

== Cloud initialization and template managment

Everything starts with a template.

You first need to have an ISO of the system you want to install (ex: archlinux.iso). You then create a VM using this ISO :

 nocloud template create archlinux archlinux.iso

Once the machine created, it will start and you will have to make your template corresponding to the following standards :

- VM must be accessible through ssh (ssh service enabled and configured on 22 port)
- ssh key (found in /usr/local/etc/nocloud.pub) must be added to /root/.ssh/authorized_keys
- network is using dhcp (dhcp service is enabled)
- python is better to be installed as ansible is the tool of choice to operate on those VMs

You can create as much templates as you want.

== Template downloading

You can download existing templates using this :

 nocloud template download archlinux

available templates can be listed with -h option

== Clone generation

=== Manual

Once your template is good, you can use it to generate new VMs :

 nocloud run machines archlinux:2:arch

will create two new machines, fresh copies from the template. Those machines will have generated names and the template's disk is set to read-only before creating the clones. The clones will run without graphical interface.

In order to organize your VMs they are grouped. This is done using a two level hierarchy :

- context will represent a kind of platform, a set of machines you use for a service.
- types will be subgroups of servers that will group VMs by function.

For instance, you run your application named ''awesome'' constituted of a database server and two web servers. You would create the set with this line :

 NC_CONTEXT=awesome nocloud run machines archlinux:2:web machines debian8:1:sql

Precision on VM specifications and the NC_CONTEXT variable are given below.

=== Using description file

If you want to automate the creation of a set of VMs, you can create description files. Each line matches a vminstantiate command line parameters. Those are separated by ":" and are in the following order :

- template name
- number of clones
- type name

The group of machines will be deduced from the file name.

For instance the following file produces 2 VMs of type web and 1 VM of type sql :

 archlinux:1:sql
 archlinux:2:web

You can specify cpu and memory for each line using the following syntax :

 archlinux:1:sql:mem=1024;cpu=4
 archlinux:2:web:mem=512

You can also add additional disks for VMs with the dsk option (sizes in GB) :

 archlinux:1:sql:mem=1024;cpu=4;dsk=5,5
 archlinux:2:web:mem=512

If you want your machines to have more human-friendly names (instead of UUIDs), specify a name prefix :

 archlinux:1:sql:mem=1024;cpu=4;name=db
 archlinux:2:web:mem=512;name=web

This will create a server called db00 for the first line and two servers on the second, called web00 and web01.

The file (named pftest) is called with the following command :

 nocloud run file pftest

And so the machines will be in the default context.

Contexts and types, besides being structural in the VM directory structure, and for naming purpose, will be used for instance if you configure those machines with ansible. Once the previous instanciation has been done, you can use dynamic inventory :

 nocloud --list
 {
   "context_sql" : {
     "hosts" : [  "192.168.1.176", ],
   },
   "context_web" : {
     "hosts" : [  "192.168.1.19", "192.168.1.23", ],
   },
   "context" : {
     "children" : [ "context_sql", "context_web", ],
     "vars": {
       "ansible_ssh_common_args": "-o StrictHostKeyChecking=no",
       "ansible_user": "root",
     },
   },
 }

You can then stop and destroy your VMs using :

 nocloud destroy

Alternatively, you can launch your description file using foreground mode

 nocloud run file pftest foreground

It will stay in foreground and log (hopefuly) useful information until you press ^C which will make it kill and destroy all its machines.

== A beautiful picture

[source]
----





    internet --------
        |           |
        |           v
        |        __________        ___________
        v       /          \      |           |
       iso --> |  template  | --> | TEMPLATES |
                \__________/      |___________|      _______
                                        |           |$>     |
                                        |           |       |
                    _____               |           |_______|
                   /     \              |          /         \
                  |  run  | <------------         /___________\
                   \____ /                            |
                      |        ________               v
                      |      _|______  |           _____
          ________    -->  _|______  | |          /     \
         /        \       |        | |_| <------ | vmssh |
        |   stop   | ---> |   VM   |_|            \____ /
         \________/   |   |________|
                      |           |
          ________    |           v                                   _______
         /        \   |            __________                        |      |\
        |   snap   | --           /           \                      |   VM |_\
         \________/              |  inventory  |  -----------------> |  list   |
                                  \___________/                      |         |
                                                                     | - vm1   |
                                                                     | - vm2   |
                                                                     |_________|

----

== nocloud commands


=== template - templates creation and downloading

template manages templates. It has two main functions : creating templates and downloading templates.

You can download templates using the following form :

----
nocloud template download TEMPLATE_NAME
----

Available templates are displayed by the -h option :

----
nocloud template help
----

You can create your own template with :

----
nocloud template create TEMPLATE_NAME ISO_NAME
----

* ISO_NAME is either the name of a template in configured iso directory, or a path to a file.
* TEMPLATE_NAME must not be the name of an existing template.

You can get the templates list with :

----
nocloud template list
----

=== run - run all the VMs of a context

run launches VMs from templates, using spec files or inline specifications. It can also rerun an existing context.

To run from a spec file and the default context :

----
nocloud run file conffile
----

to run directly from the command line :

----
nocloud run machines debian8:2:web machines debian8:1:sql
----

More complete description of VM specifications can be found above in this documentation.

=== inventory - list all the running guests on the host

vminventory is used to list running VMs.

To get running VMs :

----
nocloud inventory
----

nocloud also provides an ansible dynamic inventory, using the --list option. You can then call ansible or ansible-playbook using the script as inventory :

----
ln -s /usr/local/bin/nocloud inventory
ansible-playbook -i inventory playbook.yml
----

the scope of inventory is limited to a single context. This affects standard behaviour and --list option, but you can have all running machines using the 'all' context

----
NC_CONTEXT=all nocloud inventory
----

=== stop & destroy - halting a context

To stop all VMs of the context :

----
nocloud stop
----

To do the same and destroy all VMs in the process :

----
nocloud destroy
----

=== snap - manage snapshots

This command allows four actions described below.

==== Take a snapshot

----
nocloud snap take tst00
----

Depending on actual VM state, activity, memory size, etc, action will take some time or will not.

==== List snapshots

----
nocloud snap list tst00
ID        TAG                 VM SIZE                DATE       VM CLOCK
--        vm-20171122144532      207M 2017-11-22 14:45:32   00:00:46.787
--        vm-20171122144609      207M 2017-11-22 14:46:09   00:01:21.461
--        vm-20171122145147      207M 2017-11-22 14:51:47   00:05:10.725
--        vm-20171122145358      207M 2017-11-22 14:53:58   00:07:19.164
----

Snapshots named vm-YYYYmmddHHMMSS are those taken on a running VM. Snapshots named vd-YYYYmmddHHMMSS were taken on a stopped VM.

==== Restore snapshot

----
nocloud snap restore tst00 vm-20171122145147
----

Restoring a vm snapshot results in a running VM, whatever its original state was. Restoring a vd- snapshot results in a stopped VM, here again, whatever current state is.

==== Delete a snapshot

----
nocloud snap delete tst00 vm-20171122145147
----
=== console - accessing VM console

To access the console of a VM (its screen) :

----
nocloud console VM_NAME
----

This will only be possible if you have spicy installed.

=== qemu - accessing qemu menu and mount disks

This option lets you access qemu control menu for a VM :

----
nocloud qemu VM_NAME
----

It will also help mounting isos and floppies :

* insert a floppy disk that you'll have to mount in the VM

----
 nocloud qemu srv00 floppy file.img
 nocloud qemu srv00 iso file.iso
----

* eject an iso or a floppy by using "none" as the iso of floppy file name

----
 nocloud qemu srv00 floppy none
 nocloud qemu srv00 iso none
----

== other tools

=== vmssh - connect to VMs

vmssh connects you to a VM usgin ssh. You can name VM by its name or IP address. It also can take a command as argument, as ssh does.

----
vmssh 192.168.1.12 hostname
vmssh tst00
----

=== vmupgrade - OS upgrade

vmupgrade runs a system upgrade on a template if it knows how. It currently knows how to upgrade archlinux, debian and ubuntu systems.

----
vmupgrade debian8
----

NOTE : This is not to be used if any VM is actually based on the template. Or it will make it unusable.

=== vmnat - access specific ports

Virtual machines are accessible from the host, but if you want to expose services to outside world, you will have to nat a host port to a guest port.

Creation NAT rule :

----
vmnat 8080:tst00:80
----

above command will create an iptables nat rule to access the 80 port of the guest through the port 8080 on the host. To delete this rule, simply use the -d paramter :

----
vmnat -d 8080:tst00:80
----
