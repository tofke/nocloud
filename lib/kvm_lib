#!/usr/bin/env bash

#    (c) 2016-2017, n0vember <n0vember@half-9.net>
#
#    This file is part of nocloud.
#
#    nocloud is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    nocloud is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with nocloud.  If not, see <http://www.gnu.org/licenses/>.

# This is kvm implementation of the nocloud lib

vmBaseDir="$HOME/.local/share/nocloud/kvm"
vmDiskExt="img"
vmConfExt="conf"
vmPidExt="pid"
vmSystemType=$(uname -m)

vmTemplates="archlinux debian7 debian8 debian9 centos7"
vmTemplatesDetails="
archlinux;http://n0vember.free.fr/nocloud/templates/archlinux;098d8c6b62789d55aa0057e1466b36ab;8
debian7;http://n0vember.free.fr/nocloud/templates/debian7;6d9099a5de9f489f6128895da987a32a;6
debian8;http://n0vember.free.fr/nocloud/templates/debian8;c400e208c5a5e0208bb675d3cbe2c234;8
debian9;http://n0vember.free.fr/nocloud/templates/debian9;bcc03403831e594d5811d3f7775378ba;5
centos7;http://n0vember.free.fr/nocloud/templates/centos7;6d706ad889d59a755d68d6367f47fa68;6
"

mkdir -p ${vmBaseDir}/${vmCreateContext}

get_uuid() {
  if [ -f /proc/sys/kernel/random/uuid ] ; then
    cat /proc/sys/kernel/random/uuid
  else
    printf "%2x%2x-%2x-%2x-%2x-%2x%2x%2x" $RANDOM $RANDOM $RANDOM $RANDOM $RANDOM $RANDOM $RANDOM $RANDOM
  fi
}

vm_pid_file() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  local vmPid="$(vm_dir ${vmName} ${vmContext} ${vmType})/${vmName}.${vmPidExt}"
  echo ${vmPid}
}

vm_conf_file() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  local vmConf="$(vm_dir ${vmName} ${vmContext} ${vmType})/${vmName}.${vmConfExt}"
  echo ${vmConf}
}

vm_context_dir() {
  local vmContext=$1
  local vmType=$2

  local vmContextDir="${vmBaseDir}/${vmContext}/${vmType}"
  [ -z "${vmType}" ] && vmContextDir="${vmBaseDir}/${vmContext}"
  echo "${vmContextDir}"
}

vm_dir() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  local vmContextDir=$(vm_context_dir ${vmContext} ${vmType})
  local vmDir="${vmContextDir}/${vmName}"
  echo "${vmDir}"
}

vm_disks() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  local vmConfFile=$(vm_conf_file ${vmName} ${vmContext} ${vmType})
  local vmDisk
  local vmExtraDisk
  eval $(grep ^vmDisk= ${vmConfFile})
  eval $(grep ^vmExtraDisk= ${vmConfFile})
  echo -n "${vmDisk}"
  [ -n "${vmExtraDisk}" ] && echo -n " ${vmExtraDisk}" | tr "," " "
  echo
}

vm_disk_create() {
  local vmName=$1
  local vmCreateContext=$2
  local vmCreateType=$3
  local vmCreateDiskSize=$4

  local vmDir=$(vm_dir ${vmName} ${vmCreateContext} ${vmCreateType})
  [ -n "${vmCreateType}" ] && vmCreateContext=${vmCreateContext}/${vmCreateType}
  local vmDiskCount=0
  while [ -f "${vmDir}/${vmName}_${vmDiskCount}.${vmDiskExt}" ] ; do vmDiskCount=$((vmDiskCount+1)) ; done
  local vmDisk="${vmName}_${vmDiskCount}.${vmDiskExt}"
  if [ -f "${vmDir}/${vmDisk}" ]
  then
    echo "${vmDisk}"
  else
    mkdir -p "$vmDir" || usage "unable to create ${vmDir}"
    qemu-img create -q -f qcow2 "${vmDir}/${vmDisk}" ${vmCreateDiskSize}G >/dev/null >&2
    ret=$?
    [ $ret -eq 0 ] && echo "${vmDisk}" || usage "unable to create ${vmDir}/${vmDisk}"
  fi
}

vm_stop() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  local vmDir=$(vm_dir ${vmName} ${vmContext} ${vmType})
  vm_console_command ${vmName} quit
  rm $(vm_pid_file ${vmName} ${vmContext} ${vmType})
}

vm_run() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3
  local isoName=$4
  local vmGui=$5

  local isoOpt=""
  [ -f "${isoDir}/${isoName}" ] && isoOpt="-cdrom ${isoDir}/${isoName}"
  [ -f "${isoName}" ] && isoOpt="-cdrom ${isoName}"
  [ "${isoName} " == "none " ] && isoOpt=""

  local vmNoGui="-display sdl -vga virtio -no-quit"

  local spicePort=$(free_port 1)
  mkdir -p /tmp/nocloud.${USER}/${vmContext}/${vmType}/
  local vmMonitor="-monitor unix:/tmp/nocloud.${USER}/${vmContext}/${vmType}/${vmName},server,nowait"
  local vmSpice="-spice port=${spicePort},addr=localhost,password=${vmSpicePassword}"

  local memOpt=""
  local hugePageSize=$(grep "^Hugepagesize:" /proc/meminfo | tr -s " " " " | cut -d " " -f 2)
  local hugePageFree=$(grep "^HugePages_Free:" /proc/meminfo | tr -s " " " " | cut -d " " -f 2)
  hugePageFree=$((hugePageFree*hugePageSize/1024))

  local maxCpu=$(LC_ALL=C lscpu | grep ^CPU\(s\) | tr -s " " " " | cut -d " " -f 2)
  local cpuType="host"
  #local cpuType="qemu64,+vmx"

  local vmDir=$(vm_dir ${vmName} ${vmContext} ${vmType})

  local vmUuid=$(get_uuid)
  local vmConf=$(vm_conf_file ${vmName} ${vmContext} ${vmType})
  [ -f "${vmConf}" ] && . "${vmConf}"
  [ ${vmMem} -le ${hugePageFree} ] && memOpt="-mem-path /dev/hugepages"

  [ "${vmGui} " == "gui " ] || vmNoGui="--display none"

  [ -n "${vmExtraDisk}" ] && vmExtraDisk=$(
    echo "${vmExtraDisk}" | \
    sed -r 's/^/</;s/$/>/;s/,/> </g' |
    sed "s:<:-drive file=${vmDir}/:g" | \
    sed 's/>/,format=qcow2,if=virtio/g' \
  )

  local pidFile=$(vm_pid_file ${vmName} ${vmContext} ${vmType})
  if [ -f ${pidFile} ] ; then
    local pid=$(cat ${pidFile})
    ps ${pid} >/dev/null 2>&1
    if [ $? -eq 0 ] ; then
      log_error "a VM named ${vmName} already exists with pid ${pid}"
      return
    else
      rm ${pidFile}
    fi
  fi

  local vmMac=$(vm_mac_gen ${vmUuid})

  local kvmOpt="-enable-kvm"
  [ 0${NC_NO_KVM} -eq 1 ] && kvmOpt=""
  [ 0${NC_NO_KVM} -eq 1 ] && cpuType="qemu64"

  qemu-system-${vmSystemType} \
    ${kvmOpt} \
    ${isoOpt} \
    ${memOpt} \
    -pidfile ${pidFile} \
    -boot order=d \
    -drive file="${vmDir}/${vmDisk}",format=qcow2,if=virtio ${vmExtraDisk} \
    -cpu ${cpuType} \
    -smp cpus=${vmCpu},cores=1,maxcpus=${maxCpu} \
    -m ${vmMem} \
    -name ${vmName} \
    -uuid ${vmUuid} \
    -D "${vmDir}/${vmName}.log" \
    --daemonize \
    ${vmNoGui} ${vmMonitor} ${vmSpice} \
    -net nic,model=virtio,macaddr=${vmMac} -net bridge,br=${bridgeInterface} \
    ${vmExtraOpts} \
    >/dev/null 2>&1
}

vm_mac_gen() {
  [ $# -gt 0 ] && uuid=$1 || uuid=$(get_uuid)
  echo ${uuid} | sed -r 's/-//g;s/^(.{6}).*/005056\1/;s/(..)/\1:/g;s/:$//'
}

vm_get_mac() {
  local vmName=$1

  pgrep -f qemu-system-${vmSystemType} -a | grep " .name ${vmName}" | sed 's/.*macaddr=//;s/ .*$//'
}

vm_create() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3
  local isoName=$4
  local guiNoGui=$5

  local gui="gui"

  local vmUuid=$(get_uuid)
  local vmDir=$(vm_dir ${vmName} ${vmContext} ${vmType})
  local vmConf=$(vm_conf_file ${vmName} ${vmContext} ${vmType})
  [ -d ${vmDir} ] || mkdir -p ${vmDir}
  vmDisk="$(vm_disk_create ${vmName} ${vmContext} "${vmType}" ${vmCreateDiskSize})"

  cat > ${vmConf} <<EOF
vmName=${vmName}
vmMem=${vmMem}
vmCpu=${vmCpu}
vmDisk=${vmDisk}
vmUuid=${vmUuid}
EOF
  [ "${guiNoGui}" == "no" ] && gui=""
  vm_run ${vmName} ${vmContext} "${vmType}" "${isoName}" ${gui}
}

vm_clone_disk() {
  local vmSrcContext=$1
  local vmSrcType=$2
  local vmSrc=$3
  local vmDestContext=$4
  local vmDestType=$5
  local vmDest=$6

  local vmSrcDir=$(vm_dir ${vmSrc} ${vmSrcContext} ${vmSrcType})
  local vmSrcConf=$(vm_conf_file ${vmSrc} ${vmSrcContext} ${vmSrcType})
  local vmSrcCount=$(ps -A -o args | grep qemu-system-${vmSystemType} | grep -v grep | grep "${vmSrcDir}/" | wc -l)
  [ ${vmSrcCount} -ne 0 ] && usage "can't clone a running vm"
  . ${vmSrcConf}
  [ -f "${vmSrcDir}/${vmDisk}" ] || usage "can't clone non-existing disk ${vmSrcDir}/${vmDisk}"
  chmod a-w "${vmSrcDir}/${vmDisk}"

  local vmDestDir=$(vm_dir ${vmDest} ${vmDestContext} ${vmDestType})
  local vmDestDiskCount=0
  while [ -f "${vmDestDir}/${vmDest}_${vmDestDiskCount}.${vmDiskExt}" ] ; do vmDestDiskCount=$((vmDestDiskCount+1)) ; done
  local vmDestDisk="${vmDest}_${vmDestDiskCount}.${vmDiskExt}"
  mkdir -p "${vmDestDir}" || usage "unable to create ${vmDestDir}"
  qemu-img create -q -f qcow2 -b "${vmSrcDir}/${vmDisk}" "${vmDestDir}/${vmDestDisk}" >/dev/null >&2
  echo "${vmDestDisk}"
  set +x
}

vm_running() {
  local contextName="$1"
  local typeName="$2"

  [ "${contextName}" == "*" ] && contextName='[^/]*'
  [ "${typeName}" == "*" ] && typeName='[^/]*'
  [ "${contextName}" == "${vmCreateContext}" ] && unset typeDir
  typeDir=$(vm_context_dir ${contextName} ${typeName})

  for vmPath in $(pgrep -f qemu-system-${vmSystemType} -a | grep "file=${typeDir}/" | sed -r "s:^.* file\=${vmBaseDir}/::;s:/[^/]+,.*::")
  do
    vmName=$(echo ${vmPath} | sed -r 's:.*/([^/]+)$:\1:')
    vmContext=$(echo ${vmPath} | sed -r "s:/${vmName}::")
    vmMac=$(vm_get_mac ${vmName})
    vmIP=$(grep " ${vmMac} " /var/lib/misc/dnsmasq.leases | cut -d " " -f 3)
    echo ${vmName} ${vmContext} ${vmIP}
  done
}

vm_configure() {
  local vmName=$1

  local vmIp=$(vm_ip ${vmName})
  vm_ssh ${vmIp} "echo ${vmName} > /etc/hostname ; hostname -F /etc/hostname"
  vm_ssh ${vmIp} "echo ${vmIp} ${vmName}.nocloud ${vmName} >> /etc/hosts"
  vm_ssh ${vmIp} "pgrep dhcpcd && dhcpcd -n"
}

vm_instantiate() {
  local vmTemplate=$1
  local vmCount=$2
  local vmContext=$3
  local vmType=$4
  local vmOpts=$5

  [ -z ${vmContext} ] && vmContext=${vmDefaultContext}
  [ -z ${vmType} ] && vmType=${vmDefaultType}

  local vmDir=$(vm_context_dir ${vmContext} ${vmType})
  local vmTemplateDir=$(vm_dir ${vmTemplate} ${vmCreateContext})
  local vmTemplateConf=$(vm_conf_file ${vmTemplate} ${vmCreateContext})

  local memOpt=${tmplt_vmMem}
  local cpuOpt=${tmplt_vmCpu}
  local dskOpt=
  local guiOpt=
  local nameOpt=
  if [ -n "${vmOpts}" ] ; then
    for vmOpt in ${vmOpts}
    do
      echo ${vmOpt} | grep ^name= >/dev/null 2>&1 && nameOpt=$(echo $vmOpt | sed 's/^name=//')
      echo ${vmOpt} | grep ^mem= >/dev/null 2>&1 && memOpt=$(echo $vmOpt | sed 's/^mem=//')
      echo ${vmOpt} | grep ^cpu= >/dev/null 2>&1 && cpuOpt=$(echo $vmOpt | sed 's/^cpu=//')
      echo ${vmOpt} | grep ^dsk= >/dev/null 2>&1 && dskOpt=$(echo $vmOpt | sed 's/^dsk=//')
      echo ${vmOpt} | grep ^gui  >/dev/null 2>&1 && guiOpt="gui"
    done
  fi

  . <(sed 's/^/tmplt_/' ${vmTemplateConf})

  local vmList=
  local vmIdx=0
  for vmNum in $(seq 1 ${vmCount})
  do
    local vmUuid=$(get_uuid)
    local vmName=${vmUuid}
    [ ${vmIdx} -lt 10 ] && vmIdx="0${vmIdx}"
    [ -n "${nameOpt}" ] && vmName=${nameOpt}${vmIdx}
    [ -f ${vmDir}/${vmName} ] || mkdir -p ${vmDir}/${vmName}
    local vmDisk=$(vm_clone_disk ${vmCreateContext} "" ${vmTemplate} ${vmContext} ${vmType} ${vmName})
    [ -z ${memOpt} ] && memOpt=${tmplt_vmMem}
    [ -z ${cpuOpt} ] && cpuOpt=${tmplt_vmCpu}
    local dskList=
    if [ -n "${dskOpt}" ] ; then
      for dskSize in $(echo ${dskOpt} | tr ',' ' ')
      do
        dskName=$(vm_disk_create ${vmName} ${vmContext} ${vmType} ${dskSize})
        [ -z "${dskList}" ] && dskList="${dskName}" || dskList="${dskList},${dskName}"
      done
    fi
    local vmConf=$(vm_conf_file ${vmName} ${vmContext} ${vmType})
    cat > ${vmConf} <<EOF
vmName=${vmName}
vmUuid=${vmUuid}
vmMem=${memOpt}
vmCpu=${cpuOpt}
vmDisk=${vmDisk}
vmExtraDisk=${dskList}
vmGui=${guiOpt}
EOF
    vm_run ${vmName} ${vmContext} ${vmType} none ${guiOpt}
    vmList="${vmList} ${vmName}"
    vmIdx=$(echo ${vmIdx} | sed 's/^0*//')
    vmIdx=$((vmIdx+1))
  done

  for vmName in ${vmList}
  do
    vm_configure ${vmName} >/dev/null 2>&1 &
  done

  #wait

  for vmName in $vmList
  do
    echo ${vmName}
  done
}

is_vm_running() {
  local vmName=$1
  local vmContext=$2
  local vmType=$3

  [ -z "${vmContext}" ] && vmContext="*"
  [ -z "${vmType}" ] && vmType="*"
  local vmIsRunning=$(vm_running "${vmContext}" "${vmType}" | grep "^${vmName} " | wc -l)
  echo ${vmIsRunning}
  [ ${vmIsRunning} -eq 1 ] && return 0 || return 1
}

vm_ssh() {
  local vmIp=$1
  local vmIpTest=$(echo "${vmIp}" | sed -r '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/!d')
  [ "${vmIpTest}" != "${vmIp}" ] && vmIp=$(vm_ip ${vmIp})
  shift

  grep "^${vmIp}[ ,]" ~/.ssh/known_hosts >/dev/null && ssh-keygen -R ${vmIp} >/dev/null
  sshTry=0
  sshTryMax=30
  while [ ${sshTry} -le ${sshTryMax} ]
  do
    ssh -o StrictHostKeyChecking=false -o CheckHostIp=False -o UserKnownHostsFile=/dev/null root@${vmIp} true >/dev/null 2>&1 && break
    sleep 1
    sshTry=$((${sshTry}+1))
  done
  [ ${sshTry} -gt ${sshTryMax} ] && return 1
  ssh -o StrictHostKeyChecking=false -o CheckHostIp=False -o UserKnownHostsFile=/dev/null -o LogLevel=quiet root@${vmIp} "$@"
}

vm_ip() {
  local vmName=$1

  vmIp=
  ipFindingIteration=0
  ipFindingIterationMax=30
  while [ ${ipFindingIteration} -le ${ipFindingIterationMax} ]
  do
    vmIp=$(vm_running "*" "*" | grep ^${vmName} | cut -d ' ' -f 3)
    [ -n "${vmIp}" ] && break
    sleep 1
    ipFindingIteration=$((${ipFindingIteration}+1))
  done
  [ ${ipFindingIteration} -gt ${ipFindingIterationMax} ] && return 1
  echo $vmIp
}

vm_name() {
  local vmIp=$1

  vmName=
  nameFindingIteration=0
  nameFindingIterationMax=30
  while [ ${nameFindingIteration} -le ${nameFindingIterationMax} ]
  do
    vmName=$(vm_running "*" "*" | grep ${vmIp}$ | cut -d ' ' -f 1)
    [ -n "${vmName}" ] && break
    sleep 1
    nameFindingIteration=$((${nameFindingIteration}+1))
  done
  [ ${nameFindingIteration} -gt ${nameFindingIterationMax} ] && return 1
  echo $vmName
}

vm_context() {
  local vmName=$1
  local runningVmByName=$(vm_running "*" "*" | grep ${vmName} | wc -l)

  if [ ${runningVmByName} -gt 0 ] ; then
    vm_running "*" "*" | grep ${vmName} | cut -d ' ' -f 2 | cut -d / -f 1
  else
    local vmByName=$(ls -d ${vmBaseDir}/*/*/${vmName} | wc -l)
    [ ${vmByName} -eq 0 ] && error "No VM named ${vmName}"
    [ ${vmByName} -gt 1 ] && error "There are multiple VMs named ${vmName}"
    (cd ${vmBaseDir} ; ls -d */*/${vmName} | sed 's:/.*::')
  fi
}

vm_type() {
  local vmName=$1
  local runningVmByName=$(vm_running "*" "*" | grep ${vmName} | wc -l)

  if [ ${runningVmByName} -gt 0 ] ; then
    vm_running "*" "*" | grep ${vmName} | cut -d ' ' -f 2 | cut -d / -f 2
  else
    local vmByName=$(ls -d ${vmBaseDir}/*/*/${vmName} | wc -l)
    [ ${vmByName} -eq 0 ] && error "No VM named ${vmName}"
    [ ${vmByName} -gt 1 ] && error "There are multiple VMs named ${vmName}"
    local vmContext=$(vm_context ${vmName})
    (cd ${vmBaseDir}/${vmContext} ; ls -d */${vmName} | sed 's:/.*::')
  fi
}

vm_console_socket() {
  local vmName=$1

  pgrep -f qemu-system-${vmSystemType} -a | grep ${vmName} | sed 's/^.*-monitor //;s/ .*$//' | cut -d, -f1 | cut -d: -f2
}

vm_console_command() {
  local vmName=$1
  shift
  local vmCmd=$@
  local vmSocket=$(vm_console_socket "${vmName}")
  if [ "${vmCmd}" != "" ] ; then
    echo "${vmCmd}" | socat - ${vmSocket} | tail -n +2 | grep -v "^(qemu)"
  else
    cat | socat - ${vmSocket}
  fi
}

vm_spice_port() {
  local vmName=$1

  pgrep -f qemu-system-${vmSystemType} -a | grep ${vmName} | sed 's/^.*-spice port=//;s/,.*$//'
}

vm_context_destroy() {
  local contextName=$1

  local typeName="*"
  local vmName="*"
  rm ${vmBaseDir}/${contextName}/${typeName}/${vmName}/*
  rmdir ${vmBaseDir}/${contextName}/${typeName}/${vmName}
  rmdir ${vmBaseDir}/${contextName}/${typeName} 2>/dev/null
  rmdir ${vmBaseDir}/${contextName}/ 2>/dev/null
}
